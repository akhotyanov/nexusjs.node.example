var express = require('express');
var app = express();

app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
app.use(express.bodyParser());

var arr = new Array();

app.get('/', function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "X-Requested-With");
    response.send("<h1>NodeJS Twitter Light API is Running...</h1>");
    response.end();
});

app.get('/TwitterLight/GetAllTweets', function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "X-Requested-With");
    response.send(arr);
    response.end();
});

app.post('/TwitterLight/SaveTweet', function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "X-Requested-With");
    arr.push(request.body);
    response.end();
});

app.use('/public', express.static(__dirname + '/public'));

var port = 3000;
app.listen(port, function() {
    console.log("Listening on " + port);
});