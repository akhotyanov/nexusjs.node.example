/*
 Copyright 2012 Alexander Khotyanov
 http://NexusJS.com/
 @NexusJS

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

define(function(){

    var $N = {
        RenderView: function(view, option){
            var includes = ['jquery','mustache'];
            if (view.template) includes.push("text!" + view.template);

            require(includes, function($, Mustache, tpl) {
                if (view.template){
                    var data = view.data || {};
                    var html = Mustache.to_html(tpl, data);
                    if (option && option == 'APPEND'){
                        $(view.placeholder).append(html);
                    }else{
                        $(view.placeholder).html(html);
                    }
                }
                if (view.onLoad){
                    view.onLoad();
                }
            });
        },
        Ajax: function(ajaxCall){
            require(['jquery'], function($) {
                $.ajax({
                    url: ajaxCall.url,
                    type: ajaxCall.type,
                    data: ajaxCall.data,
                    dataType: ajaxCall.dataType || 'json',
                    contentTypeString: ajaxCall.contentTypeString || 'application/json',
                    success:  ajaxCall.success || function(){},
                    error:  ajaxCall.error || function(){}
                });
            });
        },
        CreateCommandBus: function(commandHandlersArray, globalCommandHandlersArray){
            var CommandBus = function(commandHandlersArray, globalCommandHandlersArray){
                var self = this;
                self.commandHandlers = commandHandlersArray;
                self.globalCommandHandlersArray = globalCommandHandlersArray;
                self.dispatch = function(command){
                    self.globalCommandHandlersArray.map(function(globalCommandHandler){
                        globalCommandHandler.commandAction(command);
                    });
                    var cmdHandlersCount = self.commandHandlers.length;
                    for(var i = 0; i < cmdHandlersCount; i++){
                        var commandHandler = self.commandHandlers[i];
                        if (commandHandler.commandName == command.commandName){
                            commandHandler.commandAction(command);
                            return;
                        }
                    }
                };
                self.registerGlobalCommandHandler = function(globalCommandHandler){
                    var alreadyRegistered = false;
                    self.globalCommandHandlersArray.map(function(registeredGlobalCommandHandler){
                        if(globalCommandHandler.id == registeredGlobalCommandHandler.id){
                            alreadyRegistered = true;
                        }
                    });
                    if (!alreadyRegistered){
                        self.globalCommandHandlersArray.push(globalCommandHandler);
                    }
                };
                self.registerCommandHandler = function(commandHandler){
                    var alreadyRegistered = false;
                    self.commandHandlers.map(function(registeredCommandHandler){
                        if(commandHandler.id == registeredCommandHandler.id){
                            alreadyRegistered = true;
                        }
                    });
                    if (!alreadyRegistered){
                        self.commandHandlers.push(commandHandler);
                    }
                };
            };
            return new CommandBus(commandHandlersArray, globalCommandHandlersArray);
        },
        CreateEventBus: function(eventHandlersArray, globalEventHandlersArray){
            var EventBus = function(eventHandlersArray, globalEventHandlersArray){
                var self = this;
                self.eventHandlers = eventHandlersArray;
                self.globalEventHandlers = globalEventHandlersArray;
                self.publish = function(evt){
                    if (!$N.Aggregate.isRehydrating){
                        var eventHandlersCount = self.eventHandlers.length;
                        for(var i = 0; i < eventHandlersCount; i++){
                            var eventHandler = self.eventHandlers[i];
                            if (eventHandler.eventName == evt.eventName){
                                eventHandler.eventAction(evt);
                            }
                        }
                        self.globalEventHandlers.map(function(globalEventHandler){
                            globalEventHandler.eventAction(evt);
                        });
                    }
                };
                self.registerGlobalEventHandler = function(globalEventHandler){
                    var alreadyRegistered = false;
                    self.globalEventHandlers.map(function(registeredGlobalEventHandler){
                        if(globalEventHandler.id == registeredGlobalEventHandler.id){
                            alreadyRegistered = true;
                        }
                    });
                    if (!alreadyRegistered){
                        self.globalEventHandlers.push(globalEventHandler);
                    }
                };
                self.registerEventHandler = function(eventHandler){
                    var alreadyRegistered = false;
                    self.eventHandlers.map(function(registeredEventHandler){
                        if(eventHandler.id == registeredEventHandler.id){
                            alreadyRegistered = true;
                        }
                    });
                    if (!alreadyRegistered){
                        self.eventHandlers.push(eventHandler);
                    }
                };
                self.isRegistered = function(eventHandler){
                    for (var i=0;i<self.eventHandlers.length;i++){
                        if(self.eventHandlers[i].id == eventHandler.id){
                            return true;
                        }
                    }
                    return false;
                };
            };
            return new EventBus(eventHandlersArray, globalEventHandlersArray);
        },
        NewID: function() {
            var S4 = function() {return (((1+Math.random())*0x10000)|0).toString(16).substring(1);};
            var separator = "_";
            return (S4()+S4()+separator+S4()+separator+S4()+separator+S4()+separator+S4()+S4()+S4());
        },
        Bind: function(obj){
            if (!obj.id){
                obj.id = $N.NewID();
            }
            if(obj.eventAction && obj.eventName){
                $N.EventBus.registerEventHandler(obj);
            }else if(obj.commandAction && obj.commandName){
                $N.CommandBus.registerCommandHandler(obj);
            }else if(obj.eventAction && !obj.eventName){
                $N.EventBus.registerGlobalEventHandler(obj);
            }else if(obj.commandAction && !obj.commandName){
                $N.CommandBus.registerGlobalCommandHandler(obj);
            }

        }
    };

    $N.CommandBus = $N.CreateCommandBus([],[]);
    $N.EventBus = $N.CreateEventBus([],[]);
    $N.Aggregate = { isRehydrating: false };
    $N.Dispatch = $N.CommandBus.dispatch;
    $N.Publish = $N.EventBus.publish;

    return $N;


});


