require.config({
  paths: {
    'jquery': 'lib/jquery',
    'mustache': 'lib/mustache',
    'text': 'lib/text',
    'Nexus': 'lib/nexus'
  }
});

require([
	'Nexus',
    'app/js/commands/NavigateToHomepage'
], function(
	Nexus,
    NavigateToHomepage
) {
    Nexus.Dispatch(NavigateToHomepage.Command());
});


