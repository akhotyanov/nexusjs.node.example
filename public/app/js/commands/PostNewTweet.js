define(['Nexus'],function(Nexus){

    var PostNewTweet = {
        commandName: "com.NexusJS.TwitterLight.PostNewTweet",
        Command: function(Text){
            return {
                Text: Text,
                commandName: PostNewTweet.commandName
            };
        }
    };

    Nexus.Bind({
        commandName: PostNewTweet.commandName,
        commandAction: function(cmd){
            require(['app/js/model/Tweet'], function(Tweet){
                new Tweet(cmd.Text).post();
            })
        }
    });

    return PostNewTweet;

});
