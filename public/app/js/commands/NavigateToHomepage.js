define(['Nexus'],function(Nexus){

    var NavigateToHomepage = {
        commandName: "com.NexusJS.NavigateToHomepage",
        Command: function(){
            return {
                commandName: NavigateToHomepage.commandName
            };
        }
    };

    Nexus.Bind({
        commandName: NavigateToHomepage.commandName,
        commandAction: function(){
            require(['app/js/results/HomepageShown'], function(HomepageShown){
                Nexus.Publish(HomepageShown.Event());
            })
        }
    });

    return NavigateToHomepage;

});
