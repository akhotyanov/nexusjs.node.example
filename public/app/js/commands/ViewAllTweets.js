define(['Nexus'],function(Nexus){

    var ViewAllTweets = {
        commandName: "com.NexusJS.TwitterLight.ViewAllTweets",
        Command: function(){
            return {
                commandName: ViewAllTweets.commandName
            };
        }
    };

    Nexus.Bind({
        commandName: ViewAllTweets.commandName,
        commandAction: function(){
            require(['app/js/model/Tweet','app/js/results/AllTweetsShown'], function(Tweet, AllTweetsShown){

                Nexus.Ajax({
                    type: 'GET',
                    url: 'http://localhost:3000/TwitterLight/GetAllTweets',
                    success: function(tweets){
                        Nexus.Publish(AllTweetsShown.Event(tweets));
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            })
        }
    });

    return ViewAllTweets;

});