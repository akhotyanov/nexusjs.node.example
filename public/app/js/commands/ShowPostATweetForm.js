define(['Nexus'],function(Nexus){

    var ShowPostATweetForm = {
        commandName: "com.NexusJS.TwitterLight.ShowPostATweetForm",
        Command: function(){
            return {
                commandName: ShowPostATweetForm.commandName
            };
        }
    };

    Nexus.Bind({
        commandName: ShowPostATweetForm.commandName,
        commandAction: function(){
            require(['app/js/results/PostATweetFormShown'], function(PostATweetFormShown){
                Nexus.Publish(PostATweetFormShown.Event());
            })
        }
    });

    return ShowPostATweetForm;

});