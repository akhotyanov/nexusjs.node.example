define(['Nexus'],function(Nexus){

    var Tweet = function(text, id){
        var self = this;
        self.text = text;
        self.id = id || Nexus.NewID();
        self.post = function(){
            require(['app/js/results/TweetSaved', 'app/js/results/ThanksForYourTweetPageShown'], function(TweetSaved, ThanksForYourTweetPageShown){
                Nexus.Publish(TweetSaved.Event({id:self.id, text:self.text}));
                Nexus.Publish(ThanksForYourTweetPageShown.Event('Thanks for your tweet', self.text));
            })
            return self;
        };

        return self;
    };

    return Tweet;


});