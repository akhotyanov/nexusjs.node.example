define(['Nexus'],function(Nexus){

    var PostATweetFormShown = {
        eventName: "com.NexusJS.TwitterLight.PostATweetFormShown",
        Event: function(){
            return {
                eventName: PostATweetFormShown.eventName
            };
        }
    };

    Nexus.Bind({
        eventName: PostATweetFormShown.eventName,
        eventAction: function(){
            Nexus.RenderView({
                template: 'app/html/PostATweetForm.html',
                placeholder: '#content',
                data: {},
                onLoad: function(){
                    require(['app/js/commands/NavigateToHomepage','app/js/commands/PostNewTweet'
                    ], function(NavigateToHomepage, PostNewTweet){
                        $('#PostATweet').click(function(){
                            Nexus.Dispatch(PostNewTweet.Command($('#tweet').val()));
                        });
                        $('#Homepage').click(function(){
                            Nexus.Dispatch(NavigateToHomepage.Command());
                        });
                    });
                }
            });
        }
    });

    return PostATweetFormShown;

});