define(['Nexus'],function(Nexus){

    var ThanksForYourTweetPageShown = {
        eventName: "com.NexusJS.TwitterLight.ThanksForYourTweetPageShown",
        Event: function(Message, Tweet){
            return {
                Message: Message,
                Tweet: Tweet,
                eventName: ThanksForYourTweetPageShown.eventName
            };
        }
    };

    Nexus.Bind({
        eventName: ThanksForYourTweetPageShown.eventName,
        eventAction: function(result){
            Nexus.RenderView({
                template: 'app/html/ThanksForYourTweet.html',
                placeholder: '#content',
                data: {
                    message: result.Message,
                    tweet: result.Tweet
                },
                onLoad: function(){
                    require(['app/js/commands/ViewAllTweets','app/js/commands/NavigateToHomepage','app/js/commands/ShowPostATweetForm'
                    ], function(ViewAllTweets, NavigateToHomepage, ShowPostATweetForm){
                        $('#ViewTweets').click(function(){
                            Nexus.Dispatch(ViewAllTweets.Command());
                        });
                        $('#PostTweets').click(function(){
                            Nexus.Dispatch(ShowPostATweetForm.Command());
                        });
                        $('#Homepage').click(function(){
                            Nexus.Dispatch(NavigateToHomepage.Command());
                        });
                    });
                }
            });
        }
    });

    return ThanksForYourTweetPageShown;

});