define(['Nexus'],function(Nexus){

    var TweetSaved = {
        eventName: "com.NexusJS.TwitterLight.TweetSaved",
        Event: function(Tweet){
            return {
                Tweet: Tweet,
                eventName: TweetSaved.eventName
            };
        }
    };

    Nexus.Bind({
        eventName: TweetSaved.eventName,
        eventAction: function(evt){
            Nexus.Ajax({
                type: 'POST',
                url: 'http://localhost:3000/TwitterLight/SaveTweet',
                data: {
                    tweet: evt.Tweet
                }
            });
        }
    });

    return TweetSaved;

});