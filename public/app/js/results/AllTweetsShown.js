define(['Nexus'],function(Nexus){

    var AllTweetsShown = {
        eventName: "com.NexusJS.TwitterLight.AllTweetsShown",
        Event: function(Tweets){
            return {
                Tweets: Tweets,
                eventName: AllTweetsShown.eventName
            };
        }
    };

    Nexus.Bind({
        eventName: AllTweetsShown.eventName,
        eventAction: function(result){
            Nexus.RenderView({
                template: 'app/html/ViewTweets.html',
                placeholder: '#content',
                data: {tweets: result.Tweets},
                onLoad: function(){
                    require(['app/js/commands/NavigateToHomepage'], function(NavigateToHomepage){
                        $('#Homepage').click(function(){
                            Nexus.Dispatch(NavigateToHomepage.Command());
                        });
                    });
                }
            });
        }
    });

    return AllTweetsShown;

});