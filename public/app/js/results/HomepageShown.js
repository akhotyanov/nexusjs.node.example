define(['Nexus'],function(Nexus){

    var  HomepageShown = {
        eventName:  "com.NexusJS.HomepageShown",
        Event: function(){
            return {
                eventName: HomepageShown.eventName
            };
        }
    };

    Nexus.Bind({
        eventName: HomepageShown.eventName,
        eventAction: function(){
            Nexus.RenderView({
                template: 'app/html/Homepage.html',
                placeholder: '#content',
                data: {},
                onLoad: function(){
                    require(['app/js/commands/ViewAllTweets', 'app/js/commands/ShowPostATweetForm'], function(ViewAllTweets, ShowPostATweetForm){
                        $('#ViewTweets').click(function(){
                            Nexus.Dispatch(ViewAllTweets.Command());
                        });
                        $('#PostTweets').click(function(){
                            Nexus.Dispatch(ShowPostATweetForm.Command());
                        });
                    });
                }
            });
        }
    });

    return HomepageShown;

});